package com.gamesys;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Configuration constants holder.
 */
public class Constants {
    public final static int PERIOD;
    public final static String PLAYERS_FILENAME;
    public final static boolean TRANSFER_BETS;
    public final static String GAME_END;

    static {
        PropertiesConfiguration configuration = null;
        try {
            configuration = new PropertiesConfiguration("config.properties");
        } catch (ConfigurationException e) {
            System.err.println("Unexpected eror occured while reading app configuration: " + e.toString());
        }

        PLAYERS_FILENAME = configuration.getString("players.filename");
        PERIOD = configuration.getInt("roulette.spinning.period");
        TRANSFER_BETS = configuration.getBoolean("roulette.transfer.bets");
        GAME_END = configuration.getString("game.end.signal");
    }
}
