package com.gamesys;


import com.gamesys.console.ConsoleBetReader;
import com.gamesys.console.ConsoleWriter;
import com.gamesys.file.FileUtils;
import com.gamesys.service.RouletteService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


/**
 * Entry point for Console Roulette.
 * Starts up game thread, initialize players and config properties.
 */
public class RouletteRunner {
    private static ConsoleWriter consoleWriter = new ConsoleWriter();
    //TODO: game strategy, interfaces, writer:
    // https://bitbucket.org/rainwaters/coding-exercise/src/c78b56001fce3b2aaa0abe70b0741edf3836b4b7/src/main/java/gamesys/console/ConsoleReader.java?at=master&fileviewer=file-view-default

    public static void main(String[] args) throws IOException {

        System.out.println("Starting up Console Roulette");

        try {
            List<String> players = readPlayers();

            RouletteService rouletteService = new RouletteService(players, Constants.TRANSFER_BETS, consoleWriter);

            rouletteService.printRoundStart();

            rouletteService.startRoulette(rouletteService);

            new ConsoleBetReader(rouletteService).readBets();

        } catch (Exception e) {
            consoleWriter.println("Unexpected eror occured: " + e.toString());
            e.printStackTrace();
        }

        consoleWriter.println("Stopping Console Roulette");
    }

    private static List<String> readPlayers() throws FileNotFoundException {
        List<String> players = FileUtils.readFile(Constants.PLAYERS_FILENAME);
        consoleWriter.println("Players in game: " + players);
        return players;
    }


}
