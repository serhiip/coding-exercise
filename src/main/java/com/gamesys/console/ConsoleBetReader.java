package com.gamesys.console;

import com.gamesys.Constants;
import com.gamesys.model.Bet;
import com.gamesys.service.RouletteService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

/**
 * Reads bets from console until end game command is not entered.
 */
public class ConsoleBetReader {

    private InputStream input = System.in;

    private RouletteService rouletteService;

    public ConsoleBetReader(RouletteService rouletteService) {
        this.rouletteService = rouletteService;
    }

    public void readBets() throws IOException {
        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(input));
            String inputLine = br.readLine();
            if (Constants.GAME_END.equals(inputLine)) {
                rouletteService.stopRoulette();
                return;
            }
            Optional<Bet> bet = Bet.fromConsoleLine(inputLine);
            bet.ifPresent(rouletteService::registerBet);
        }
    }

    public void setInput(InputStream input) {
        this.input = input;
    }
}
