package com.gamesys.console;


import java.io.PrintStream;

/**
 * Writes data to console.
 */
public class ConsoleWriter {

    private PrintStream printer = System.out;

    public void printf(String format, Object... args) {
        printer.printf(format, args);
    }

    public void println(String string) {
        printer.println(string);
    }

    public void setPrinter(PrintStream printer) {
        this.printer = printer;
    }
}
