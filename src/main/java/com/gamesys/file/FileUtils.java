package com.gamesys.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by strange on 13.02.2016.
 */
public class FileUtils {

    public static List<String> readFile(String filename) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(filename));
        List<String> lines = new ArrayList<>();
        while (sc.hasNextLine()) {
            lines.add(sc.nextLine());
        }
        return lines;
    }
}
