package com.gamesys.model;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.Optional;

/**
 * Roulette bet model.
 *
 * Created by strange on 13.02.2016.
 */
public class Bet {

    private String playerName;
    private double amount;
    private byte number;
    private PARITY parity;

    public enum PARITY {
        EVEN,
        ODD
    }

    public static Optional<Bet> fromConsoleLine(String betLine) {
        try {
            Bet bet = new Bet();
            String[] betData = betLine.split("\\s+");
            assert betData.length == 3;
            bet.setPlayerName(betData[0]);
            if (NumberUtils.isNumber(betData[1])) {
                byte number = Byte.parseByte(betData[1]);
                assert number >= 1 && number <= 36;
                bet.setNumber(number);
            } else {
                bet.setParity(PARITY.valueOf(betData[1]));
            }
            bet.setAmount(Double.parseDouble(betData[2]));
            return Optional.of(bet);
        } catch (Exception e) {
            System.out.println("Cant parse bet: " + betLine + " due to: " + e.toString());
            return Optional.empty();
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public PARITY getParity() {
        return parity;
    }

    public void setParity(PARITY parity) {
        this.parity = parity;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Bet{");
        sb.append("playerName='").append(playerName).append('\'');
        sb.append(", amount=").append(amount);
        if(parity != null) {
            sb.append(", parity=").append(parity);
        } else {
            sb.append(", number=").append(number);
        }
        sb.append('}');
        return sb.toString();
    }
}
