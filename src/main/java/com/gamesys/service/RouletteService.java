package com.gamesys.service;

import com.gamesys.Constants;
import com.gamesys.console.ConsoleWriter;
import com.gamesys.model.Bet;
import com.gamesys.tasks.RouletteTask;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Roulette game operations service.
 * Responsible for registering players, bets, win/loose logic.
 *
 * Created by strange on 13.02.2016.
 */
public class RouletteService {
    private boolean transferBets;
    private ConsoleWriter consoleWriter;

    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
    private ConcurrentMap<String, Bet> bets = new ConcurrentHashMap<>();
    private List<String> players;

    public RouletteService(List<String> players, boolean transferBets, ConsoleWriter consoleWriter) {
        this.players = players;
        this.transferBets = transferBets;
        this.consoleWriter = consoleWriter;
    }

    public void startRoulette(RouletteService rouletteService) {
        executorService.scheduleWithFixedDelay(new RouletteTask(rouletteService), Constants.PERIOD, Constants.PERIOD, TimeUnit.SECONDS);
    }

    public void stopRoulette() {
        executorService.shutdown();
    }

    /**
     * Place bet.
     *
     * @param bet bet to register
     */
    public void registerBet(Bet bet) {
        if (!players.contains(bet.getPlayerName())) {
            consoleWriter.println("Player is not registered, bet is rejected: " + bet);
            return;
        }

        Bet previousBet = bets.put(bet.getPlayerName(), bet);
        if(previousBet == null) {
            consoleWriter.println("New bet registered: " + bet);
        } else{
            consoleWriter.println("Bet replaced. Old value: " + previousBet + ", new value: " + bet);
        }

    }

    public ConcurrentMap<String, Bet> getBets() {
        return bets;
    }

    /**
     *  Process win/loose logic on round end.
     *
     * @param rouletteNumber round winning number
     */
    public void processBets(int rouletteNumber) {
        printRoundHeader(rouletteNumber);

        bets.entrySet().stream().forEach(bet -> processBet(bet.getValue(), rouletteNumber));

        if (!transferBets) {
            bets.clear();
        }
    }

    public void printRoundStart() {
        consoleWriter.println("Spinning... ");
        consoleWriter.println("Ladies and gentlemen, please place your bets");
    }

    private void printRoundHeader(int rouletteNumber) {
        consoleWriter.println("Number: " + rouletteNumber);
        consoleWriter.printf("%-10s %-10s %-10s %-10s%n", "Player", "Bet", "Outcome", "Winnings");
        consoleWriter.println("---");
    }

    private void processBet(Bet bet, int rouletteNumber) {
        if (bet.getParity() != null) {
            boolean even = rouletteNumber % 2 == 0;
            if (bet.getParity().equals(Bet.PARITY.EVEN) && even) {
                betWonOnParity(bet);
            } else if (bet.getParity().equals(Bet.PARITY.ODD) && !even) {
                betWonOnParity(bet);
            } else {
                betLoose(bet);
            }
        }
        if (bet.getNumber() != 0) {
            if (bet.getNumber() == rouletteNumber) {
                betWonOnNumber(bet);
            } else {
                betLoose(bet);
            }
        }
    }

    private void betWonOnNumber(Bet bet) {
        print(bet, "WIN", bet.getAmount() * 36);
    }

    private void betLoose(Bet bet) {
        print(bet, "LOSE", 0.0);
    }

    private void betWonOnParity(Bet bet) {
        print(bet, "WIN", bet.getAmount() * 2);
    }

    private void print(Bet bet, String outcome, double winnings) {
        consoleWriter.printf("%-10s %-10s %-10s %-10s%n", bet.getPlayerName(), bet.getAmount(), outcome, winnings);
    }

}
