package com.gamesys.tasks;

import com.gamesys.service.RouletteService;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Roulette spinning task that chooses roulette winning number randomly.
 *
 * Created by strange on 13.02.2016.
 */
public class RouletteTask implements Runnable {
    public static final byte ROULETTE_MIN = 0;
    public static final byte ROULETTE_MAX = 36;

    private RouletteService rouletteService;
    private ThreadLocalRandom localRandom = ThreadLocalRandom.current();

    public RouletteTask(RouletteService rouletteService) {
        this.rouletteService = rouletteService;
    }

    @Override
    public void run() {
        int rouletteNumber = localRandom.nextInt(ROULETTE_MIN, ROULETTE_MAX + 1);
        rouletteService.processBets(rouletteNumber);
        rouletteService.printRoundStart();
    }


}
