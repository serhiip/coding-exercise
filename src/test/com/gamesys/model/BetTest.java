package com.gamesys.model;

import org.junit.Test;

import java.util.Optional;

import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class BetTest {

    @Test
    public void shouldNotParseLowerCaseParity() throws Exception {
        assertFalse(Bet.fromConsoleLine("testPlayer odd 2.0").isPresent());
    }

    @Test(expected = AssertionError.class)
    public void shouldNotParseWrongFormattedString() throws Exception {
        Bet.fromConsoleLine("test Player odd 2.0");
    }

    @Test
    public void shouldNotParseWrongFormattedString2() throws Exception {
        assertFalse(Bet.fromConsoleLine("bad string example").isPresent());
    }

    @Test
    public void shouldParseCorrectBetParity() throws Exception {
        Optional<Bet> bet = Bet.fromConsoleLine("testPlayer ODD 2.0");
        assertTrue(bet.isPresent());
        assertEquals(2.0, bet.get().getAmount(), 0);
        assertEquals("testPlayer", bet.get().getPlayerName());
        assertEquals(Bet.PARITY.ODD, bet.get().getParity());
    }

    @Test
    public void shouldParseCorrectBetNumber() throws Exception {
        Optional<Bet> bet = Bet.fromConsoleLine("testPlayer 5 2.0");
        assertTrue(bet.isPresent());
        assertEquals(2.0, bet.get().getAmount(), 0);
        assertEquals("testPlayer", bet.get().getPlayerName());
        assertEquals(5, bet.get().getNumber());
        assertNull(bet.get().getParity());
    }
}