package com.gamesys.service;

import com.gamesys.console.ConsoleWriter;
import com.gamesys.model.Bet;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;

public class RouletteServiceTest {

    ConsoleWriter consoleWriter = new ConsoleWriter();
    RouletteService rouletteService = new RouletteService(Arrays.asList("testPlayer"), false, consoleWriter);

    @Test
    public void shouldRegisterCorrectBet() throws Exception {
        rouletteService.registerBet(Bet.fromConsoleLine("testPlayer ODD 2.0").get());
        assertNotNull(rouletteService.getBets().get("testPlayer"));
    }

    @Test
    public void shouldNotRegisterBetWithUnregiteredPlayName() throws Exception {
        rouletteService.registerBet(Bet.fromConsoleLine("unknownPlayer ODD 2.0").get());
        assertTrue(rouletteService.getBets().isEmpty());
    }

    @Test
    public void testProcessBetsOdd() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        consoleWriter.setPrinter(new PrintStream(out));

        rouletteService.registerBet(Bet.fromConsoleLine("testPlayer ODD 2.0").get());

        rouletteService.processBets(2);

        String s = new String(out.toByteArray());
        assertEquals("New bet registered: Bet{playerName='testPlayer', amount=2.0, parity=ODD}\n" +
                    "Number: 2\n" +
                    "Player     Bet        Outcome    Winnings  \n" +
                    "---\n" +
                    "testPlayer 2.0        LOSE       0.0       \n", s);
    }

    @Test
    public void testProcessBetsNumber() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        consoleWriter.setPrinter(new PrintStream(out));

        rouletteService.registerBet(Bet.fromConsoleLine("testPlayer 6 2.0").get());

        rouletteService.processBets(6);

        String s = new String(out.toByteArray());
        assertEquals("New bet registered: Bet{playerName='testPlayer', amount=2.0, number=6}\n" +
                    "Number: 6\n" +
                    "Player     Bet        Outcome    Winnings  \n" +
                    "---\n" +
                    "testPlayer 2.0        WIN        72.0      \n", s);
    }
}